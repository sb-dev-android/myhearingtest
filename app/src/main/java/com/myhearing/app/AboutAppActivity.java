package com.myhearing.app;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageView;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Hitesh Gehlot on 3/5/16.
 */
public class AboutAppActivity extends BaseActivity {

    @Bind(R.id.back_button)
    ImageView backButton;
    @Bind(R.id.webview)
    WebView webview;

    @OnClick(R.id.back_button)
    public void clickbackbutton() {
        finish();
    }

    @Override
    public void OnSuperSuccess(String response, String key) {

    }

    @Override
    public void OnSuperFailure(String errormsg) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutapp);
        ButterKnife.bind(this);
        webview.getSettings().setJavaScriptEnabled(true);
        if (Locale.getDefault().getLanguage().equalsIgnoreCase("es")) {
            webview.loadUrl("file:///android_asset/about_app_Spanish.html");
        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("it")) {
            webview.loadUrl("file:///android_asset/about_app_Italian.html");
        } else {
            webview.loadUrl("file:///android_asset/about_app.html");
        }
    }

}
