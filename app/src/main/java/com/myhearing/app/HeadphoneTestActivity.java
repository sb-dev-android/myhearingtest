package com.myhearing.app;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.myhearing.app.Application.Myapplication;
import com.myhearing.app.CustomWidget.AppleBold;
import com.myhearing.app.CustomWidget.VerdanaBold;
import com.myhearing.app.CustomWidget.VerdanaRegular;
import com.myhearing.app.Fragment.Menufragment;
import com.myhearing.app.SoundMeter.SoundMeter;
import com.myhearing.app.ToneGenerator.ContinuousBuzzer;
import com.myhearing.app.Wrapper.ToneWrapper;
import com.navdrawer.SimpleSideDrawer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Hitesh Gehlot on 4/5/16.
 */
public class HeadphoneTestActivity extends BaseActivity implements ContinuousBuzzer.OnCompletionListner {


    @Bind(R.id.buttonimg)
    ImageButton buttonimg;
    @Bind(R.id.btnstarttest)
    ImageView btnstarttest;
    public SimpleSideDrawer slide;
    @Bind(R.id.titletext)
    AppleBold titletext;
    @Bind(R.id.txt_left)
    VerdanaRegular txtLeft;
    @Bind(R.id.txt_right)
    VerdanaRegular txtRight;
    @Bind(R.id.title_description)
    VerdanaBold titleDescription;

    @OnClick(R.id.img_menu)
    public void clickMenu() {
        slide.toggleLeftDrawer();
    }


    @OnClick(R.id.back_button)
    public void clickbackbutton() {
        finish();
    }

    @OnClick(R.id.home_button)
    public void clickhomebutton() {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    Boolean Isrighttest = false;
    private static final String TAG = "HeadphonesStart";

    ArrayList<ToneWrapper> tonelist;

    List<Integer> shufferlist;
    ArrayList<HashMap<String, Integer>> hashMapslist;

    boolean headphone_select = false;
    BroadcastReceiver myReceiver;
    private static final int POLL_INTERVAL = 300;
    Boolean rightenable = false;

    @OnClick(R.id.buttonimg)
    public void clickImageButton() {
        HashMap<String, Integer> integerIntegerHashMap = new HashMap<>();
        if (LoopPosition > 3) {
            integerIntegerHashMap.put(tonelist.get(LoopPosition).getToneFrequency() + "L",
                    tonelist.get(LoopPosition).getdBwrappers().get(dbtone).getDb());
        } else {
            integerIntegerHashMap.put(tonelist.get(LoopPosition).getToneFrequency() + "R",
                    tonelist.get(LoopPosition).getdBwrappers().get(dbtone).getDb());
        }
        hashMapslist.add(integerIntegerHashMap);
        LoopPosition++;
        dbtone = -1;
        startnexttone();
    }

    @OnClick(R.id.btnstarttest)
    public void clickStartTest() {

        if (!headphone_select) {
            Myapplication.popErrorMsg(getResources().getString(R.string.app_name), getString(R.string.headphone_connetrion), HeadphoneTestActivity.this);
            return;
        }
        if (Isrighttest && rightenable) {
            Isrighttest = false;
            btnstarttest.setImageResource(R.drawable.stop_button);
            buttonimg.setEnabled(true);
//            LoopPosition++;
            dbtone = -1;
            startnexttone();
            return;
        }

        if (!IsStartTest) {
            popstarttest(getResources().getString(R.string.app_name), getString(R.string.start_headphone_test_msg), HeadphoneTestActivity.this);
        } else {
            IsStartTest = false;
            btnstarttest.setImageResource(R.drawable.start_button);
            if (buzzer != null)
                buzzer.tryStopPlayer();
            IntilizeObject();
        }
    }

    private void popstarttest(String titleMsg, String errorMsg,
                              Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titleMsg).setMessage(errorMsg)
                .setPositiveButton(getResources().getString(R.string.start), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        IsStartTest = true;
                        buttonimg.setEnabled(true);
                        btnstarttest.setImageResource(R.drawable.stop_button);
                        starttone();
                    }
                }).setNegativeButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    int dbtone = 0;


    private void starttone() {
        if (LoopPosition < 8) {
            if (buzzer != null && buzzer.isPlaying()) {
                buzzer.tryStopPlayer();
            }
//            System.out.println("Playing tone:" + tonelist.get(LoopPosition).getToneFrequency() + "Hrz   " + tonelist.get(LoopPosition).getdBwrappers().get(dbtone).getDb() + "db");
//            mp = MediaPlayer.create(this, tonelist.get(LoopPosition).getdBwrappers().get(dbtone).getTone());
//            mp.setOnCompletionListener(this);
//            mp.start();
            buzzer = new ContinuousBuzzer();
            buzzer.setOnCompletionListner(this);
            buzzer.setPauseTimeInMs(5000);
            buzzer.setToneFreqInHz(tonelist.get(LoopPosition).getToneFrequency());
            buzzer.setVolume(tonelist.get(LoopPosition).getdBwrappers().get(dbtone).getDbvolume());
            System.out.println("mode:" + tonelist.get(LoopPosition).getMode());
            System.out.println("LoopPosition:" + LoopPosition);
            buzzer.setMode(tonelist.get(LoopPosition).getMode());
            buzzer.play();
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if (IsStartTest)
//                        startnexttone();
//                }
//            }, 4500);
        } else {
            if (buzzer != null)
                buzzer.tryStopPlayer();
            Intent intent = new Intent(this, SpeakerGraphActivity.class);
            intent.putExtra("hashMapslist", hashMapslist);
            startActivity(intent);
            IntilizeObject();
//            IntilizeObject();

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (buzzer != null)
            buzzer.tryStopPlayer();
        IntilizeObject();
        unregisterReceiver(myReceiver);
    }


    @Override
    public void onResume() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(myReceiver, filter);
        initializeApplicationConstants();

        if (!mRunning) {
            mRunning = true;
            start();
        }
        super.onResume();
    }


    private void startnexttone() {
        dbtone++;
        if (LoopPosition > 7) {
            if (buzzer != null)
                buzzer.tryStopPlayer();
            Intent intent = new Intent(this, SpeakerGraphActivity.class);
            intent.putExtra("hashMapslist", hashMapslist);
            startActivity(intent);
            IntilizeObject();
            IsStartTest = false;
            return;
        }
        if (LoopPosition > 3) {
            txtLeft.setVisibility(View.GONE);
            txtRight.setVisibility(View.VISIBLE);
            buttonimg.setImageResource(R.drawable.button_left);

            if (!Isrighttest && LoopPosition == 4 && !rightenable) {
                Isrighttest = true;
                rightenable = true;
                btnstarttest.setImageResource(R.drawable.start_button);
                buttonimg.setEnabled(false);
                buzzer.pause();
                Myapplication.popErrorMsg(getResources().getString(R.string.alert), getString(R.string.left_ear_headphone), this);
                return;
            }
        }
        if (dbtone > 3 && LoopPosition == 3) {
            txtLeft.setVisibility(View.GONE);
            txtRight.setVisibility(View.VISIBLE);
            buttonimg.setImageResource(R.drawable.button_left);

            if (!Isrighttest && !rightenable) {
                Isrighttest = true;
                rightenable = true;
                btnstarttest.setImageResource(R.drawable.start_button);
                buttonimg.setEnabled(false);
                buzzer.pause();
                Myapplication.popErrorMsg(getResources().getString(R.string.alert), getString(R.string.left_ear_headphone), this);
                return;
            }
        }
        if (dbtone > 3) {
            HashMap<String, Integer> integerIntegerHashMap = new HashMap<>();
            dbtone = 3;
            if (LoopPosition > 3) {
                integerIntegerHashMap.put(tonelist.get(LoopPosition).getToneFrequency() + "L",
                        tonelist.get(LoopPosition).getdBwrappers().get(dbtone).getDb());
            } else {
                integerIntegerHashMap.put(tonelist.get(LoopPosition).getToneFrequency() + "R",
                        tonelist.get(LoopPosition).getdBwrappers().get(dbtone).getDb());
            }
            hashMapslist.add(integerIntegerHashMap);

            LoopPosition++;
            dbtone = 0;
        }
        starttone();
    }


//    MediaPlayer mp;

    private boolean mRunning = false;
    /**
     * config state
     **/
    private int mThreshold;

    private PowerManager.WakeLock mWakeLock;
    private SoundMeter mSensor;
    int LoopPosition = 0;
    Handler mHandler;
    boolean IsStartTest = false;
    private AudioManager audioManager = null;
    ContinuousBuzzer buzzer;

    @Override
    public void OnSuperSuccess(String response, String key) {

    }

    @Override
    public void OnSuperFailure(String errormsg) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        setContentView(R.layout.activity_headphonetest);
        buzzer = new ContinuousBuzzer();
        ButterKnife.bind(this);
        titletext.setText(getResources().getString(R.string.test));
        titleDescription.setText(getResources().getString(R.string.Test_using_headphones));
        IntilizeObject();
        myReceiver = new MusicIntentReceiver();
        audioManager = (AudioManager) getSystemService(SetVolumeActivity.AUDIO_SERVICE);

        mHandler = new Handler();
        mSensor = new SoundMeter();
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "NoiseAlert");
        slide = new SimpleSideDrawer(this);
        slide.setLeftBehindContentView(R.layout.menu_layout);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.framelayout, new Menufragment());
        ft.commit();
    }


    private void IntilizeObject() {
        shufferlist = new ArrayList<>();
        shufferlist.add(0);
        shufferlist.add(1);
        shufferlist.add(2);
        shufferlist.add(3);
        hashMapslist = new ArrayList<>();
        Collections.shuffle(shufferlist);
        LoopPosition = 0;
        dbtone = 0;
        tonelist = new ArrayList<>();

        for (int i = 0; i < shufferlist.size(); i++) {
            ToneWrapper toneWrapper = new ToneWrapper();
            toneWrapper.setdBwrappers(Myapplication.getTonelist().get(shufferlist.get(i)).getdBwrappers());
            toneWrapper.setToneFrequency(Myapplication.getTonelist().get(shufferlist.get(i)).getToneFrequency());
            toneWrapper.setMode("Right");
            tonelist.add(toneWrapper);
        }
        for (int i = 0; i < shufferlist.size(); i++) {
            ToneWrapper toneWrapper = new ToneWrapper();
            toneWrapper.setdBwrappers(Myapplication.getTonelist().get(shufferlist.get(i)).getdBwrappers());
            toneWrapper.setToneFrequency(Myapplication.getTonelist().get(shufferlist.get(i)).getToneFrequency());
            toneWrapper.setMode("Left");
            tonelist.add(toneWrapper);
        }
        IsStartTest = false;
        btnstarttest.setImageResource(R.drawable.start_button);
        Isrighttest = false;
        rightenable = false;
        buttonimg.setImageResource(R.drawable.button_right);

        txtLeft.setVisibility(View.VISIBLE);
        txtRight.setVisibility(View.GONE);
        buttonimg.setEnabled(false);

    }

    Boolean Ispausestatus = false;

    @Override
    public void OnCompleteAudio() {
        if (IsStartTest)
            startnexttone();
    }

    private class MusicIntentReceiver extends BroadcastReceiver {
        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0:
                        Log.d(TAG, "Headset is unplugged");
                        // btn_headphones.setEnabled(false);
                        headphone_select = false;
                        //btn_headphones.setEnabled(false);
                        //     btn_start.setAlpha((float) 0.5);
                        if (IsStartTest) {
                            if (buzzer != null && buzzer.isPlaying()) {
                                buzzer.pause();
                                Ispausestatus = true;
                            }
                            popErrorMsg(getResources().getString(R.string.alert), getString(R.string.headphone_connetrion), HeadphoneTestActivity.this);
                        }
                        break;
                    case 1:
                        Log.d(TAG, "Headset is plugged");
                        //btn_headphones.setEnabled(true);
                        //btn_headphones.setEnabled(true);
                        headphone_select = true;

                        break;
                    default:
                        Log.d(TAG, "I have no idea what the headset state is");
                }
            }
        }
    }

    private void popErrorMsg(String titleMsg, String errorMsg,
                             Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titleMsg).setMessage(errorMsg)
                .setPositiveButton(getResources().getString(R.string.continue_txt), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (headphone_select) {
                            if (Ispausestatus) {
                                Ispausestatus = false;
                                buzzer.resume();
                            }
                        } else {
                            popErrorMsg(getResources().getString(R.string.alert), getString(R.string.headphone_connetrion), HeadphoneTestActivity.this);


                        }


                    }
                }).setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (buzzer != null)
                    buzzer.tryStopPlayer();
                IntilizeObject();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    Boolean Isdialogopen = false;

    private void popErrorMsgNoise(String titleMsg, String errorMsg,
                                  Context context) {
        Isdialogopen = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titleMsg).setMessage(errorMsg)
                .setPositiveButton(getResources().getString(R.string.continue_txt), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Isdialogopen = false;
                        if (amp <= 29) {
                            if (Ispausestatus) {
                                Ispausestatus = false;
                                buzzer.resume();
                            }
                        } else {
                            popErrorMsgNoise(getResources().getString(R.string.alert), getString(R.string.test_interrupted), HeadphoneTestActivity.this);
                        }
                    }
                }).setNegativeButton(getResources().getString(R.string.cancel_test), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Isdialogopen = false;
                if (buzzer != null)
                    buzzer.tryStopPlayer();
                IntilizeObject();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onStop() {
        super.onStop();
        stop();
    }

    private void start() {
        //Log.i("Noise", "==== start ===");

        mSensor.start();
        if (!mWakeLock.isHeld()) {
            mWakeLock.acquire();
        }
        mHandler.postDelayed(mPollTask, POLL_INTERVAL);
    }

    private void stop() {
        //Log.i("Noise", "==== Stop Noise Monitoring===");
        if (mWakeLock.isHeld()) {
            mWakeLock.release();
        }
        mHandler.removeCallbacks(mSleepTask);
        mHandler.removeCallbacks(mPollTask);
        mSensor.stop();
        mRunning = false;

    }

    private Runnable mSleepTask = new Runnable() {
        public void run() {
            //Log.i("Noise", "runnable mSleepTask");
            start();
        }
    };
    double amp;
    // Create runnable thread to Monitor Voice
    private Runnable mPollTask = new Runnable() {
        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        public void run() {

            amp = mSensor.getAmplitude();
            //Log.i("Noise", "runnable mPollTask");
            System.out.println("amp=" + amp);
            System.out.println("mThresold=" + mThreshold);

            if (amp > 29) {
                if (IsStartTest) {
                    if (buzzer != null && buzzer.isPlaying()) {
                        buzzer.pause();
                        Ispausestatus = true;
                    }
                    if (!Isdialogopen) {
                        popErrorMsgNoise(getResources().getString(R.string.alert), getString(R.string.test_interrupted), HeadphoneTestActivity.this);
                    }
                }
            }
            mHandler.postDelayed(this, POLL_INTERVAL);


        }
    };

    private void initializeApplicationConstants() {
        // Set Noise Threshold
        mThreshold = 10;

    }
}
