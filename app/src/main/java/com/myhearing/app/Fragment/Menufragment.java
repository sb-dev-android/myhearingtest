package com.myhearing.app.Fragment;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.myhearing.app.AboutAppActivity;
import com.myhearing.app.AboutDeveloperActivity;
import com.myhearing.app.Application.Myapplication;
import com.myhearing.app.LanguageActivity;
import com.myhearing.app.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hitesh on 21/5/16.
 */
public class Menufragment extends Fragment {


    @Bind(R.id.aboutapp)
    LinearLayout aboutapp;
    @Bind(R.id.rateusapp)
    LinearLayout rateusapp;
    @Bind(R.id.questionfeedbackapp)
    LinearLayout questionfeedbackapp;
    @Bind(R.id.aboutdeveloperapp)
    LinearLayout aboutdeveloperapp;
    @Bind(R.id.languagebtn)
    LinearLayout languagebtn;

    @OnClick(R.id.aboutdeveloperapp)
    public void clickAboutDeveloper() {
        startActivity(new Intent(getActivity(), AboutDeveloperActivity.class));
    }

    @OnClick(R.id.questionfeedbackapp)
    public void clickQuestionfeedback() {
        final Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        final PackageManager pm = getActivity().getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
        ResolveInfo best = null;
        for (final ResolveInfo info : matches)
            if (info.activityInfo.packageName.endsWith(".gm") ||
                    info.activityInfo.name.toLowerCase().contains("gmail") ||
                    info.activityInfo.name.toLowerCase().contains("email")) best = info;
        if (best != null)
            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);

        intent.putExtra(Intent.EXTRA_SUBJECT, "My Hearing App");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@mdata.gr"});

        startActivity(intent);

    }

    @OnClick(R.id.rateusapp)
    public void clickRateUs() {
//        http://www.mdata.gr/MD/
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("market://details?id=" + Myapplication.getInstance().getPackageName()));
//        i.setData(Uri.parse("http://www.mdata.gr/MD/"));
        startActivity(i);
    }

    @OnClick(R.id.link)
    public void clicklink() {
//        http://www.mdata.gr/MD/
        Intent i = new Intent(Intent.ACTION_VIEW);
//        i.setData(Uri.parse("market://details?id=" + Myapplication.getInstance().getPackageName()));
        i.setData(Uri.parse("http://www.mdata.gr/"));

        startActivity(i);
    }

    @OnClick(R.id.aboutapp)
    public void clickAboutApp() {
        startActivity(new Intent(getActivity(), AboutAppActivity.class));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menu, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.languagebtn)
    public void onClickLanguage() {
        startActivity(new Intent(getActivity(), LanguageActivity.class));

    }
}
