package com.myhearing.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.loopj.android.http.RequestParams;
import com.myhearing.app.Application.Myapplication;
import com.myhearing.app.WebTasks.WebServiceApi;
import com.myhearing.app.WebTasks.WebServiceCallBack;

/**
 * Created by hitesh on 10/2/17.
 */

public abstract class BaseActivity extends AppCompatActivity implements WebServiceCallBack {

    static Boolean IsResumecalled = false;
    WebServiceApi webServiceApi;

    public abstract void OnSuperSuccess(String response, String key);

    public abstract void OnSuperFailure(String errormsg);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        webServiceApi = new WebServiceApi(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!IsResumecalled) {
            System.out.println("BaseActivity:onResume");
            IsResumecalled = true;
            RequestParams requestParams = new RequestParams();
            requestParams.put("app_action", "OPEN");
            requestParams.put("device_type", "A");
            requestParams.put("country_id", "72");
            webServiceApi.PostMethod(WebServiceApi.KEY_APP_ANALYSIS, WebServiceApi.API_APP_ANALYSIS, requestParams, false);
//            param: country_id,app_action( OPEN,CLOSE),device_type(A,I)
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (Myapplication.getInstance().isApplicationBroughtToBackground()) {
            System.out.println("BaseActivity:onStop");
            IsResumecalled = false;
            RequestParams requestParams = new RequestParams();
            requestParams.put("app_action", "CLOSE");
            requestParams.put("device_type", "A");
            requestParams.put("country_id", "72");
            webServiceApi.PostMethod(WebServiceApi.KEY_APP_ANALYSIS, WebServiceApi.API_APP_ANALYSIS, requestParams, false);
//            param: country_id,app_action( OPEN,CLOSE),device_type(A,I)
        }
    }


    @Override
    public void OnSuccess(String response, String key) {
        OnSuperSuccess(response, key);
    }

    @Override
    public void OnFailure(String errormsg) {
        OnSuperFailure(errormsg);
    }
}
