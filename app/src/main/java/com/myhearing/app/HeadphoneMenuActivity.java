package com.myhearing.app;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.myhearing.app.Application.Myapplication;
import com.myhearing.app.CustomWidget.AppleBold;
import com.myhearing.app.Fragment.Menufragment;
import com.navdrawer.SimpleSideDrawer;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Hitesh Gehlot on 3/5/16.
 */
public class HeadphoneMenuActivity extends BaseActivity {
    private static final String TAG = "HeadphonesStart";
    boolean headphone_select = false;
    BroadcastReceiver myReceiver;
    @Bind(R.id.img_menu)
    ImageView imgMenu;
    public SimpleSideDrawer slide;
    @Bind(R.id.titletext)
    AppleBold titletext;

    @OnClick(R.id.img_menu)
    public void clickMenu() {
        slide.toggleLeftDrawer();

    }

    @OnClick(R.id.headphone_conatiner)
    public void clickHeadphone() {
        if (!headphone_select) {

            Myapplication.popErrorMsg(getResources().getString(R.string.app_name), getString(R.string.headphone_connetrion), HeadphoneMenuActivity.this);
        } else {
            Intent i = new Intent(HeadphoneMenuActivity.this, HeadphoneTestActivity.class);
            startActivity(i);

        }
    }

    @OnClick(R.id.img_device_container)
    public void clickdevicespeaker() {
        if (headphone_select) {
            Myapplication.popErrorMsg(getResources().getString(R.string.app_name), getString(R.string.headphone_disconnetrion), HeadphoneMenuActivity.this);
            return;
        }
        Intent intent = new Intent(HeadphoneMenuActivity.this, DeviceSpeakerTutorialActivity.class);
        startActivity(intent);
    }

//    @OnClick(R.id.img_info)
//    public void clickInfo() {
//        Myapplication.popErrorMsg(getString(R.string.hearingsource), getString(R.string.hearingsource_info), this);
//    }


    @OnClick(R.id.back_button)
    public void clickbackbutton() {
        finish();
    }

    @OnClick(R.id.home_button)
    public void clickhomebutton() {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void OnSuperSuccess(String response, String key) {

    }

    @Override
    public void OnSuperFailure(String errormsg) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_headphonedevice);
        ButterKnife.bind(this);
        titletext.setText(getResources().getString(R.string.hearingsource));
        myReceiver = new MusicIntentReceiver();
        slide = new SimpleSideDrawer(this);
        slide.setLeftBehindContentView(R.layout.menu_layout);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.framelayout, new Menufragment());
        ft.commit();
    }

    private class MusicIntentReceiver extends BroadcastReceiver {
        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0:
                        Log.d(TAG, "Headset is unplugged");
                        // btn_headphones.setEnabled(false);
                        headphone_select = false;
                        //btn_headphones.setEnabled(false);
                        //     btn_start.setAlpha((float) 0.5);
                        break;
                    case 1:
                        Log.d(TAG, "Headset is plugged");
                        //btn_headphones.setEnabled(true);
                        //btn_headphones.setEnabled(true);
                        headphone_select = true;

                        break;
                    default:
                        Log.d(TAG, "I have no idea what the headset state is");
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myReceiver);
    }

    @Override
    public void onResume() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(myReceiver, filter);
        super.onResume();
    }

}
