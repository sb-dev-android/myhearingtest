package com.myhearing.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.crittercism.app.Crittercism;
import com.loopj.android.http.RequestParams;
import com.myhearing.app.Application.Myapplication;
import com.myhearing.app.WebTasks.WebServiceApi;
import com.myhearing.app.WebTasks.WebServiceCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;


/**
 * Created by Hitesh Gehlot on 3/5/16.
 */
public class SplashActivity extends BaseActivity {

    ImageView imageView;
    WebServiceApi webServiceApi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Crittercism.initialize(getApplicationContext(), "3de93273936b45cd82eeda167592145e00555300");
        imageView = (ImageView) findViewById(R.id.image);
        webServiceApi = new WebServiceApi(this, this);
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!Locale.getDefault().getLanguage().equalsIgnoreCase("es") && Myapplication.getInstance().getDeviceToken().equalsIgnoreCase(""))
                    setDeviceToken();
                else
                    popErrorMsg(getResources().getString(R.string.warning_msg), SplashActivity.this);
                return false;
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!Locale.getDefault().getLanguage().equalsIgnoreCase("es") && Myapplication.getInstance().getDeviceToken().equalsIgnoreCase(""))
                    setDeviceToken();
            }
        }, 500);

    }

    public void popErrorMsg(String errorMsg,
                            Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(getResources().getString(R.string.alert)).setMessage(errorMsg)
                .setPositiveButton(getResources().getString(R.string.i_agree), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startActivity(new Intent(SplashActivity.this, SetVolumeActivity.class));
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    String devicetoken = "";

    private void setDeviceToken() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.create().requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setMessage(getResources().getString(R.string.enter_access_code));

        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setHint(getResources().getString(R.string.enter_access_code));
        alertDialog.setView(input);

        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        devicetoken = input.getText().toString();
                        RequestParams requestParams = new RequestParams();
                        requestParams.put("uuid", Myapplication.getAndroid_id());
                        requestParams.put("token", input.getText().toString());
                        requestParams.put("device_type", "A");
                        if (Myapplication.getInstance().getLanguage().equalsIgnoreCase(Myapplication.ITALIAN)) {
                            requestParams.put("country_id", "106");
                        } else if (Myapplication.getInstance().getLanguage().equalsIgnoreCase(Myapplication.ENGLISH)) {
                            requestParams.put("country_id", "176");
                        } else if (Myapplication.getInstance().getLanguage().equalsIgnoreCase(Myapplication.ACCDEVICE)) {
                            if (Locale.getDefault().getLanguage().equalsIgnoreCase("it")) {
                                requestParams.put("country_id", "106");
                            } else {
                                requestParams.put("country_id", "176");
                            }
                        }
                        webServiceApi.PostMethod(WebServiceApi.KEY_USER_TOKEN, WebServiceApi.API_USER_TOKEN, requestParams, true);
                    }
                });

        alertDialog.show();

    }


    public void ErrorMsg(String errorMsg,
                         Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(errorMsg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        setDeviceToken();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void OnSuperSuccess(String response, String key) {
        if (key.equalsIgnoreCase(WebServiceApi.KEY_USER_TOKEN)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getInt("status") == 1) {
                    Myapplication.getInstance().setDeviceToken(devicetoken);
                } else {
                    if (jsonObject.getInt("status") == 2) {
                        ErrorMsg(getResources().getString(R.string.access_code_already_used), SplashActivity.this);
                    } else {
                        ErrorMsg(getResources().getString(R.string.access_code_not_matching), SplashActivity.this);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void OnSuperFailure(String errormsg) {
        Myapplication.popErrorMsg(getResources().getString(R.string.alert), errormsg, this);
    }
}
