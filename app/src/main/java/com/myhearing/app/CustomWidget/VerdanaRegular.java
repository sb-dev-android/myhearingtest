package com.myhearing.app.CustomWidget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Hitesh Gehlot on 3/5/16.
 */
public class VerdanaRegular extends TextView {

    private static Typeface mTypeface;

    public VerdanaRegular(final Context context) {
        this(context, null);

    }

    public VerdanaRegular(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public VerdanaRegular(final Context context, final AttributeSet attrs,
                          final int defStyle) {
        super(context, attrs, defStyle);

        if (mTypeface == null) {
            mTypeface = Typeface.createFromAsset(context.getAssets(),
                    "fonts/VERDANA_1.TTF");
        }
        setTypeface(mTypeface);
    }

}
