package com.myhearing.app;

import android.Manifest;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.widget.ImageView;

import com.myhearing.app.CustomWidget.AppleBold;
import com.myhearing.app.CustomWidget.VerdanaBold;
import com.myhearing.app.Fragment.Menufragment;
import com.navdrawer.SimpleSideDrawer;
import com.rey.material.widget.Slider;
import com.tbruyelle.rxpermissions.RxPermissions;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.functions.Action1;

/**
 * Created by Hitesh Gehlot on 3/5/16.
 */
public class SetVolumeActivity extends BaseActivity implements Slider.OnPositionChangeListener {

    @Bind(R.id.icon_ok)
    ImageView iconOk;
    @Bind(R.id.txt_percent)
    VerdanaBold txtPercent;
    @Bind(R.id.slider)
    Slider slider;
    @Bind(R.id.btncontinue)
    ImageView btncontinue;
    @Bind(R.id.titletext)
    AppleBold titletext;
    private AudioManager audioManager = null;
    int maxvolume = 0;
    public SimpleSideDrawer slide;
    private final String[] REQUEST_PERMISSIONS = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO
    };

    @OnClick(R.id.img_menu)
    public void clickMenu() {
        slide.toggleLeftDrawer();
    }

    @OnClick(R.id.back_button)
    public void clickbackbutton() {
        finish();
    }

    @OnClick(R.id.home_button)
    public void clickhomebutton() {
        finish();
    }

    @OnClick(R.id.btncontinue)
    public void clickButtonContinue() {
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                maxvolume, 0);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            Intent intent = new Intent(SetVolumeActivity.this, EnvironmentActivity.class);
            startActivity(intent);
        } else {
            RxPermissions.getInstance(this)
                    .request(REQUEST_PERMISSIONS)
                    .subscribe(new Action1<Boolean>() {
                        @Override
                        public void call(Boolean granted) {
                            if (granted) {
                                Intent intent = new Intent(SetVolumeActivity.this, EnvironmentActivity.class);
                                startActivity(intent);
                            }
                        }
                    });
        }


    }


    @Override
    public void OnSuperSuccess(String response, String key) {

    }

    @Override
    public void OnSuperFailure(String errormsg) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setvolume);
        ButterKnife.bind(this);

        titletext.setText(getResources().getString(R.string.volume));
        audioManager = (AudioManager) getSystemService(SetVolumeActivity.AUDIO_SERVICE);
        slider.setValueRange(0, 100, false);
        maxvolume = audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int progress = 40;
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                (int) ((maxvolume * progress) / 100), 0);
        slider.setValue(progress, false);
        slider.setPrimaryColor(getResources().getColor(R.color.green_text));

        btncontinue.setAlpha((float) 1.0);
        btncontinue.setEnabled(true);
        iconOk.setAlpha((float) 1.0);

//        voluSeekBar.getProgressDrawable().setColorFilter(getResources().getColor(R.color.green_text), PorterDuff.Mode.MULTIPLY);

//        voluSeekBar.setThumb(getResources().getDrawable(R.drawable.circlegreenshape));
        txtPercent.setText(progress + "%");
        slider.setOnPositionChangeListener(this);
        slide = new SimpleSideDrawer(this);
        slide.setLeftBehindContentView(R.layout.menu_layout);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.framelayout, new Menufragment());
        ft.commit();
    }


    @Override
    public void onPositionChanged(Slider view, boolean fromUser, float oldPos, float newPos, int oldValue, int newValue) {
        if (newValue != 40) {
            btncontinue.setAlpha((float) 0.5);
            btncontinue.setEnabled(false);
            iconOk.setAlpha((float) 0.5);
            slider.setPrimaryColor(getResources().getColor(R.color.red));
        } else {
            btncontinue.setAlpha((float) 1.0);
            btncontinue.setEnabled(true);
            iconOk.setAlpha((float) 1.0);
            slider.setPrimaryColor(getResources().getColor(R.color.green_text));
        }
        txtPercent.setText(newValue + "%");
    }
}
