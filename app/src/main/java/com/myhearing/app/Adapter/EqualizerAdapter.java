package com.myhearing.app.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.myhearing.app.R;


/**
 * Created by Hitesh Gehlot on 11/5/16.
 */
public class EqualizerAdapter extends BaseAdapter {


    int totalsize = 29;
    int currentequalizervalue = 5;
    LayoutInflater layoutInflater;

    public EqualizerAdapter(Context context) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return totalsize;
    }

    public void notifydata(int currentequalizervalue) {
        this.currentequalizervalue = currentequalizervalue;
        notifyDataSetChanged();
    }


    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = layoutInflater.inflate(R.layout.custom_equalizer, parent, false);
        ImageView equalizerimage = (ImageView) view.findViewById(R.id.equalizerimage);

        if (currentequalizervalue > position) {
            if (position < 5) {
                equalizerimage.setImageResource(R.mipmap.equalizer2);
            } else if (position < 10) {
                equalizerimage.setImageResource(R.mipmap.equalizer3);

            }

            else if (position < 29) {
                equalizerimage.setImageResource(R.mipmap.equalizer4);

            } else {
                equalizerimage.setImageResource(R.mipmap.equalizer_gray);

            }

        } else {
            equalizerimage.setImageResource(R.mipmap.equalizer_gray);

        }

        return view;
    }
}
