package com.myhearing.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.loopj.android.http.RequestParams;
import com.myhearing.app.Application.Myapplication;
import com.myhearing.app.WebTasks.WebServiceApi;
import com.myhearing.app.WebTasks.WebServiceCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Hitesh Gehlot on 3/5/16.
 */
public class LanguageActivity extends BaseActivity {

    @Bind(R.id.back_button)
    ImageView backButton;
    @Bind(R.id.accordingdeviceimg)
    ImageView accordingdeviceimg;
    @Bind(R.id.englishimg)
    ImageView englishimg;
    @Bind(R.id.spanishimg)
    ImageView spanishimg;
    String selectedlanguage = "";
    WebServiceApi webServiceApi;
    @Bind(R.id.italianimg)
    ImageView italianimg;

    @OnClick(R.id.back_button)
    public void clickbackbutton() {
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        ButterKnife.bind(this);
        webServiceApi = new WebServiceApi(this, this);
        selectedlanguage = Myapplication.getInstance().getLanguage();
        if (selectedlanguage.equalsIgnoreCase(Myapplication.ACCDEVICE)) {
            accordingdeviceimg.setImageResource(R.mipmap.toggle_select);
            englishimg.setImageResource(R.mipmap.toggle_unselect);
            spanishimg.setImageResource(R.mipmap.toggle_unselect);
            italianimg.setImageResource(R.mipmap.toggle_unselect);
        } else if (selectedlanguage.equalsIgnoreCase(Myapplication.ENGLISH)) {
            englishimg.setImageResource(R.mipmap.toggle_select);
            accordingdeviceimg.setImageResource(R.mipmap.toggle_unselect);
            spanishimg.setImageResource(R.mipmap.toggle_unselect);
            italianimg.setImageResource(R.mipmap.toggle_unselect);
        } else if (selectedlanguage.equalsIgnoreCase(Myapplication.SPANISH)) {
            spanishimg.setImageResource(R.mipmap.toggle_select);
            accordingdeviceimg.setImageResource(R.mipmap.toggle_unselect);
            englishimg.setImageResource(R.mipmap.toggle_unselect);
            italianimg.setImageResource(R.mipmap.toggle_unselect);
        } else if (selectedlanguage.equalsIgnoreCase(Myapplication.ITALIAN)) {
            spanishimg.setImageResource(R.mipmap.toggle_unselect);
            accordingdeviceimg.setImageResource(R.mipmap.toggle_unselect);
            englishimg.setImageResource(R.mipmap.toggle_unselect);
            italianimg.setImageResource(R.mipmap.toggle_select);
        }
    }

    @OnClick({R.id.accordingtodevicebtn, R.id.englishbtn, R.id.spanishbtn, R.id.italianbtn, R.id.savebtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.accordingtodevicebtn:
                selectedlanguage = Myapplication.ACCDEVICE;
                accordingdeviceimg.setImageResource(R.mipmap.toggle_select);
                englishimg.setImageResource(R.mipmap.toggle_unselect);
                spanishimg.setImageResource(R.mipmap.toggle_unselect);
                italianimg.setImageResource(R.mipmap.toggle_unselect);
                break;
            case R.id.englishbtn:
                selectedlanguage = Myapplication.ENGLISH;
                englishimg.setImageResource(R.mipmap.toggle_select);
                accordingdeviceimg.setImageResource(R.mipmap.toggle_unselect);
                spanishimg.setImageResource(R.mipmap.toggle_unselect);
                italianimg.setImageResource(R.mipmap.toggle_unselect);
                break;
            case R.id.spanishbtn:
                selectedlanguage = Myapplication.SPANISH;
                spanishimg.setImageResource(R.mipmap.toggle_select);
                accordingdeviceimg.setImageResource(R.mipmap.toggle_unselect);
                englishimg.setImageResource(R.mipmap.toggle_unselect);
                italianimg.setImageResource(R.mipmap.toggle_unselect);
                break;
            case R.id.italianbtn:
                selectedlanguage = Myapplication.ITALIAN;
                spanishimg.setImageResource(R.mipmap.toggle_unselect);
                accordingdeviceimg.setImageResource(R.mipmap.toggle_unselect);
                englishimg.setImageResource(R.mipmap.toggle_unselect);
                italianimg.setImageResource(R.mipmap.toggle_select);
                break;
            case R.id.savebtn:

                if (selectedlanguage.equalsIgnoreCase(Myapplication.SPANISH)) {
                    Myapplication.getInstance().changeLocale(this, new Locale("es"));
                } else if (selectedlanguage.equalsIgnoreCase(Myapplication.ITALIAN)) {
                    Myapplication.getInstance().changeLocale(this, new Locale("it"));
                } else if (selectedlanguage.equalsIgnoreCase(Myapplication.ENGLISH)) {
                    Myapplication.getInstance().changeLocale(this, new Locale("en"));
                } else {
                    if (Myapplication.getInstance().getDefaultLanguage().equalsIgnoreCase(Myapplication.SPANISH)) {
                        Myapplication.getInstance().changeLocale(this, new Locale("es"));
                    } else if (Myapplication.getInstance().getDefaultLanguage().equalsIgnoreCase(Myapplication.ITALIAN)) {
                        Myapplication.getInstance().changeLocale(this, new Locale("it"));
                    } else {
                        Myapplication.getInstance().changeLocale(this, new Locale("en"));
                    }
                }
                if (!selectedlanguage.equalsIgnoreCase(Myapplication.ACCDEVICE) && !selectedlanguage.equalsIgnoreCase("es") && Myapplication.getInstance().getDeviceToken().equalsIgnoreCase(""))
                    setDeviceToken();
                else {
                    Myapplication.getInstance().setLanguage(selectedlanguage);
                    Intent intent = new Intent(this, SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

                break;
        }
    }

    String devicetoken = "";

    private void setDeviceToken() {

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.create().requestWindowFeature(Window.FEATURE_NO_TITLE);

        alertDialog.setMessage(getResources().getString(R.string.enter_access_code));

        final EditText input = new EditText(this);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        input.setLayoutParams(lp);
        input.setHint(getResources().getString(R.string.enter_access_code));
        alertDialog.setView(input);

        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        devicetoken = input.getText().toString();
                        RequestParams requestParams = new RequestParams();
                        requestParams.put("uuid", Myapplication.getAndroid_id());
                        requestParams.put("token", input.getText().toString());
                        requestParams.put("device_type", "A");
                        if (selectedlanguage.equalsIgnoreCase(Myapplication.ITALIAN)) {
                            requestParams.put("country_id", "106");
                        } else if (selectedlanguage.equalsIgnoreCase(Myapplication.ENGLISH)) {
                            requestParams.put("country_id", "176");
                        } else if (selectedlanguage.equalsIgnoreCase(Myapplication.ACCDEVICE)) {
                            if (Locale.getDefault().getLanguage().equalsIgnoreCase("it")) {
                                requestParams.put("country_id", "106");
                            } else {
                                requestParams.put("country_id", "176");
                            }
                        }
                        webServiceApi.PostMethod(WebServiceApi.KEY_USER_TOKEN, WebServiceApi.API_USER_TOKEN, requestParams, true);
                    }
                });
        alertDialog.show();

    }


    @Override
    public void OnSuperSuccess(String response, String key) {
        if (key.equalsIgnoreCase(WebServiceApi.KEY_USER_TOKEN)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getInt("status") == 1) {
                    Myapplication.getInstance().setDeviceToken(devicetoken);
                    Myapplication.getInstance().setLanguage(selectedlanguage);
                    Intent intent = new Intent(this, SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    ErrorMsg(getResources().getString(R.string.access_code_not_matching), LanguageActivity.this);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void OnSuperFailure(String errormsg) {
        Myapplication.popErrorMsg(getResources().getString(R.string.alert), errormsg, this);
    }

    public void ErrorMsg(String errorMsg,
                         Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(errorMsg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        setDeviceToken();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


}
