package com.myhearing.app;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Hitesh Gehlot on 3/5/16.
 */
public class AboutDeveloperActivity extends BaseActivity {

    @Bind(R.id.back_button)
    ImageView backButton;

    @OnClick(R.id.back_button)
    public void clickbackbutton() {
        finish();
    }

    @Override
    public void OnSuperSuccess(String response, String key) {

    }

    @Override
    public void OnSuperFailure(String errormsg) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutdeveloper);
        ButterKnife.bind(this);
    }

}
