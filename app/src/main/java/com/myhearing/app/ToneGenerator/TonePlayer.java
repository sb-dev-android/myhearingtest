package com.myhearing.app.ToneGenerator;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Handler;
import android.util.Log;

/**
 * Created by hitesh on 17/1/17.
 */

public abstract class TonePlayer {
    protected double toneFreqInHz = 440;
    protected float volume = 100;

    protected AudioTrack audioTrack;
    protected boolean isPlaying = false;

    public String getMode() {
        return Mode;
    }

    public void setMode(String mode) {
        Mode = mode;
    }

    protected String Mode = "";
    protected Thread playerWorker;

    public TonePlayer() {
    }

    protected abstract void CompleteAudio();

    public TonePlayer(double toneFreqInHz) {
        this.toneFreqInHz = toneFreqInHz;
    }

    public void play() {
        if (isPlaying)
            return;

        stop();

        isPlaying = true;
        asyncPlayTrack(toneFreqInHz);
    }

    public void stop() {
        isPlaying = false;
        if (audioTrack == null)
            return;

        tryStopPlayer();
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public float getVolume() {
        return volume;
    }

    public double getToneFreqInHz() {
        return toneFreqInHz;
    }

    public void setToneFreqInHz(double toneFreqInHz) {
        this.toneFreqInHz = toneFreqInHz;
    }

    protected abstract void asyncPlayTrack(final double toneFreqInHz);

    public void tryStopPlayer() {
        isPlaying = false;
        try {
            if (playerWorker != null)
                playerWorker.interrupt();

            // pause() appears to be more snappy in audio cutoff than stop()
            if (audioTrack != null) {
                audioTrack.pause();
                audioTrack.flush();

                audioTrack.release();
                audioTrack = null;
            }
        } catch (IllegalStateException e) {
            // Likely issue with concurrency, doesn't matter, since means it's stopped anyway
            // so we just eat the exception
        } catch (NullPointerException e) {

        }
    }

    /**
     * below from http://stackoverflow.com/questions/2413426/playing-an-arbitrary-tone-with-android
     */
    protected void playTone(double freqInHz, double seconds, int ramp) {
        int sampleRate = 44100;

        double dnumSamples = seconds * sampleRate;
        dnumSamples = Math.ceil(dnumSamples);
        int numSamples = (int) dnumSamples;
        double sample[] = new double[numSamples];
        byte soundData[] = new byte[2 * numSamples];

        // Fill the sample array
        for (int i = 0; i < numSamples; ++i)
            sample[i] = Math.sin(freqInHz * 2 * Math.PI * i / (sampleRate));

        // convert to 16 bit pcm sound array
        // assumes the sample buffer is normalized.
        // convert to 16 bit pcm sound array
        // assumes the sample buffer is normalised.
        int idx = 0;
        int i = 0;
        ramp = numSamples / 20;
        // Amplitude ramp as a percent of sample count
        System.out.println("amp:" + ramp);

        // Ramp amplitude up (to avoid clicks)
        for (i = 0; i < ramp; ++i) {
            double dVal = sample[i];
            // Ramp up to maximum
            final short val = (short) ((dVal * 32767 * i / ramp));
            // in 16 bit wav PCM, first byte is the low order byte
            soundData[idx++] = (byte) (val & 0x00ff);
            soundData[idx++] = (byte) ((val & 0xff00) >>> 8);
        }


        // Max amplitude for most of the samples
        for (i = i; i < numSamples - ramp; ++i) {
            double dVal = sample[i];
            // scale to maximum amplitude
            final short val = (short) ((dVal * 32767));
            // in 16 bit wav PCM, first byte is the low order byte
            soundData[idx++] = (byte) (val & 0x00ff);
            soundData[idx++] = (byte) ((val & 0xff00) >>> 8);
        }

        // Ramp amplitude down
        for (i = i; i < numSamples; ++i) {
            double dVal = sample[i];
            // Ramp down to zero
            final short val = (short) ((dVal * 32767 * (numSamples - i) / ramp));
            // in 16 bit wav PCM, first byte is the low order byte
            soundData[idx++] = (byte) (val & 0x00ff);
            soundData[idx++] = (byte) ((val & 0xff00) >>> 8);
        }

        playSound(sampleRate, soundData);
    }

    protected void playSound(int sampleRate, byte[] soundData) {
        try {
            int bufferSize = AudioTrack.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
            audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                    sampleRate, AudioFormat.CHANNEL_OUT_MONO,
                    AudioFormat.ENCODING_PCM_16BIT, bufferSize,
                    AudioTrack.MODE_STREAM);
//            float gain = (float) (volume / 100.0);
            //noinspection deprecation

            if (Mode.equalsIgnoreCase("Both")) {
                audioTrack.setStereoVolume(volume, volume);
            } else if (Mode.equalsIgnoreCase("Left")) {
                audioTrack.setStereoVolume(volume, 0);
            } else if (Mode.equalsIgnoreCase("Right")) {
                audioTrack.setStereoVolume(0, volume);
            }

            audioTrack.setNotificationMarkerPosition(200000 + 10000);
//            System.out.println("@@@samplerate:" + sampleRate * 5000);
//            System.out.println("@@@datalenght:" + soundData.length / 2);

            audioTrack.setPlaybackPositionUpdateListener(new AudioTrack.OnPlaybackPositionUpdateListener() {
                @Override
                public void onPeriodicNotification(AudioTrack track) {
                    // nothing to do
                    System.out.println("@@@onPeriodicNotification");
                }

                @Override
                public void onMarkerReached(AudioTrack track) {
//                    Log.d(LOG_TAG, "Audio track end of file reached...");
//                    messageHandler.sendMessage(messageHandler.obtainMessage(PLAYBACK_END_REACHED));
                    System.out.println("@@@onMarkerReached");
                    CompleteAudio();
                }
            });

//            audioTrack.amp
            audioTrack.play();
            System.out.println("@@@play");
            audioTrack.write(soundData, 0, soundData.length);

        } catch (Exception e) {
            Log.e("tone player", e.toString());
        }
        try {
            if (audioTrack != null)
                tryStopPlayer();
        } catch (Exception ex) {
            //
        }
    }

}

