package com.myhearing.app.ToneGenerator;


import android.media.AudioTrack;

/**
 * Created by hitesh on 17/1/17.
 */

public class ContinuousBuzzer extends TonePlayer {
    protected double pausePeriodSeconds = 5;
    protected int pauseTimeInMs = 1;

    public void setOnCompletionListner(OnCompletionListner onCompletionListner) {
        this.onCompletionListner = onCompletionListner;
    }

    OnCompletionListner onCompletionListner = null;

    public void setToneDB(int toneDB) {
        this.toneDB = toneDB;
    }

    protected int toneDB = 5000;

    /**
     * The time the buzzer should pause for every cycle in milliseconds.
     */
    public int getPauseTimeInMs() {
        return pauseTimeInMs;
    }

    /**
     * The time the buzzer should pause for every cycle in milliseconds.
     */
    public void setPauseTimeInMs(int pauseTimeInMs) {
        this.pauseTimeInMs = pauseTimeInMs;
    }

    /**
     * The time period between when a buzzer pause should occur in seconds.
     */
    public double getPausePeriodSeconds() {
        return pausePeriodSeconds;
    }

    /**
     * The time period between when a buzzer pause should occur in seconds.
     * IE pause the buzzer every X/pausePeriod seconds.
     */
    public void setPausePeriodSeconds(double pausePeriodSeconds) {
        this.pausePeriodSeconds = pausePeriodSeconds;
    }

    @Override
    protected void CompleteAudio() {
        if (onCompletionListner != null)
            onCompletionListner.OnCompleteAudio();
    }

    protected void asyncPlayTrack(final double toneFreqInHz) {
        playerWorker = new Thread(new Runnable() {
            public void run() {
                while (isPlaying) {
                    // will pause every x seconds useful for determining when a certain amount
                    // of time has passed while whatever the buzzer is signaling is active
                    playTone(toneFreqInHz, pausePeriodSeconds, toneDB);
                    try {
                        Thread.sleep(pauseTimeInMs);
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        });
        playerWorker.start();
    }

    public Boolean isPlaying() {
        if (audioTrack != null && audioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
            return true;
        }
        return false;
    }


    public interface OnCompletionListner {
        public void OnCompleteAudio();
    }

    public void pause() {
        if (audioTrack != null)
            audioTrack.pause();
    }

    public void resume() {
        if (audioTrack != null)
            audioTrack.play();
    }
}

