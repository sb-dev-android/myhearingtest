package com.myhearing.app;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.meetme.android.horizontallistview.HorizontalListView;
import com.myhearing.app.Adapter.EqualizerAdapter;
import com.myhearing.app.CustomWidget.AppleBold;
import com.myhearing.app.CustomWidget.VerdanaRegular;
import com.myhearing.app.Fragment.Menufragment;
import com.myhearing.app.SoundMeter.SoundMeter;
import com.navdrawer.SimpleSideDrawer;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Hitesh Gehlot on 3/5/16.
 */
public class EnvironmentActivity extends FragmentActivity {
    /* constants */
    private static final int POLL_INTERVAL = 300;
    private static final int REQUEST_CODE_SOUND = 1;
    @Bind(R.id.HorizontalListView)
    HorizontalListView horizontalListView;
    @Bind(R.id.btncontinue)
    ImageView btncontinue;
    @Bind(R.id.environmentstatus)
    VerdanaRegular environmentstatus;
    @Bind(R.id.icon_ok)
    ImageView iconOk;
    @Bind(R.id.titletext)
    AppleBold titletext;
    /**
     * running state
     **/
    private boolean mRunning = false;
    /**
     * config state
     **/
    private int mThreshold;
    public SimpleSideDrawer slide;

    private PowerManager.WakeLock mWakeLock;
    private SoundMeter mSensor;
    EqualizerAdapter equalizerAdapter;

    Handler mHandler;


    private Runnable mSleepTask = new Runnable() {
        public void run() {
            //Log.i("Noise", "runnable mSleepTask");
            start();
        }
    };

    // Create runnable thread to Monitor Voice
    private Runnable mPollTask = new Runnable() {
        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        public void run() {

            double amp = mSensor.getAmplitude();
            double amplitudeEMA = mSensor.getAmplitudeEMA();

            //Log.i("Noise", "runnable mPollTask");
            System.out.println("amp=" + amp);
            System.out.println("amplitudeEMA=" + amplitudeEMA);

            System.out.println("mThresold=" + mThreshold);

            if (amp > 29) {
                btncontinue.setAlpha((float) 0.5);
                btncontinue.setEnabled(false);
            } else {
                btncontinue.setAlpha((float) 1);
                btncontinue.setEnabled(true);
            }

            if (amp < mThreshold) {
                environmentstatus.setText(getString(R.string.environment1));
                iconOk.setImageResource(R.mipmap.icn_check1);
            } else if (amp < 20) {
                environmentstatus.setText(getString(R.string.environment2));
                iconOk.setImageResource(R.mipmap.icn_check2);
            } else {
                environmentstatus.setText(getString(R.string.environment3));
                iconOk.setImageResource(R.mipmap.icn_check3);
            }
            if (amp < 2) {
                equalizerAdapter.notifydata(2);
            } else {
                equalizerAdapter.notifydata((int) (amp / 2));
            }
            mHandler.postDelayed(this, POLL_INTERVAL);


        }
    };

    @OnClick(R.id.img_menu)
    public void clickMenu() {
        slide.toggleLeftDrawer();

    }

    @OnClick(R.id.back_button)
    public void clickbackbutton() {
        finish();
    }

    @OnClick(R.id.home_button)
    public void clickhomebutton() {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @OnClick(R.id.btncontinue)
    public void clickButtonContinue() {
        Intent intent = new Intent(EnvironmentActivity.this, HeadphoneMenuActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_environment);
        ButterKnife.bind(this);
        titletext.setText(getResources().getString(R.string.environment));
        equalizerAdapter = new EqualizerAdapter(this);
        horizontalListView.setAdapter(equalizerAdapter);
        mHandler = new Handler();
        mSensor = new SoundMeter();
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "NoiseAlert");
        slide = new SimpleSideDrawer(this);
        slide.setLeftBehindContentView(R.layout.menu_layout);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.framelayout, new Menufragment());
        ft.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeApplicationConstants();
        if (!(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)) {
            checkCameraPermission();
        } else {
            if (!mRunning) {
                mRunning = true;
                start();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        stop();
    }

    private void start() {
        //Log.i("Noise", "==== start ===");
        mSensor.start();
        if (!mWakeLock.isHeld()) {
            mWakeLock.acquire();
        }
        mHandler.postDelayed(mPollTask, POLL_INTERVAL);
    }

    private void stop() {
        //Log.i("Noise", "==== Stop Noise Monitoring===");
        if (mWakeLock.isHeld()) {
            mWakeLock.release();
        }
        mHandler.removeCallbacks(mSleepTask);
        mHandler.removeCallbacks(mPollTask);
        mSensor.stop();
        mRunning = false;

    }


    private void initializeApplicationConstants() {
        // Set Noise Threshold
        mThreshold = 10;

    }


    private void callForHelp() {

        Toast.makeText(getApplicationContext(), "Noise Thersold Crossed, do here your stuff.",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void checkCameraPermission() {
        Log.d("mTest Permission Check", "Checking for RECORD_AUDIO permission");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO)) {
                Toast.makeText(EnvironmentActivity.this, "Permission is needed for proper working of this app", Toast.LENGTH_SHORT).show();
            } else {
                ActivityCompat.requestPermissions(
                        this, new String[]{Manifest.permission.RECORD_AUDIO},
                        REQUEST_CODE_SOUND);
            }

        } else {
            if (!mRunning) {
                mRunning = true;
                start();
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_SOUND) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (!mRunning) {
                    mRunning = true;
                    start();
                }
                Log.d("mTest Permission Check", "RECORD_AUDIO permission Granted");
            } else {
                Log.d("mTest Permission Check", "RECORD_AUDIO permission Denied");
                Toast.makeText(EnvironmentActivity.this, "Permission is needed for proper working of this app", Toast.LENGTH_SHORT).show();
            }
        }
    }
}