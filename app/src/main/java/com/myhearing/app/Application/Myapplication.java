package com.myhearing.app.Application;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.myhearing.app.R;
import com.myhearing.app.Wrapper.DBwrapper;
import com.myhearing.app.Wrapper.ToneWrapper;
import com.myhearing.app.util.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;


public class Myapplication extends Application {

    private static ProgressDialog dialog;
    private static Myapplication ctx;
    public static String AppName = "";

    public static ArrayList<ToneWrapper> getTonelist() {
        return tonelist;
    }

    public final String SHARED_PREF_NAME = "Hearing_pref";

    public static ArrayList<ToneWrapper> tonelist;
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    public static ConnectionDetector cd;
    int[] hzname = new int[]{1000, 2000, 4000, 8000};

    float volume[] = {0.003f, 0.005534f, 0.009950f, 0.017755f};
    int[] dbname = new int[]{20, 30, 40, 50};

    public String LANGUAGE = "language";
    public String DEVICETOKEN = "deviceToken";
    public String DEFAULTLANGUAGE = "defaultlanguage";
    public static final String ACCDEVICE = "device";
    public static final String ENGLISH = "english";
    public static final String SPANISH = "spanish";
    public static final String ITALIAN = "italian";

    public static String getAndroid_id() {
        return android_id;
    }

    public static String android_id;

    @Override
    public void onCreate() {
        super.onCreate();
        ctx = this;
        sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        cd = new ConnectionDetector(this);
        AppName = ctx.getResources().getString(R.string.app_name);
        tonelist = new ArrayList<>();

        for (int i = 0, k = 0; i < hzname.length; i++) {
            ToneWrapper toneWrapper = new ToneWrapper();
            toneWrapper.setToneFrequency(hzname[i]);
            ArrayList<DBwrapper> dBwrapperlist = new ArrayList<>();
            for (int j = 0; j < volume.length; j++, k++) {
                DBwrapper dBwrapper = new DBwrapper();
                dBwrapper.setDbvolume(volume[j]);
                dBwrapper.setDb(dbname[j]);
                dBwrapperlist.add(dBwrapper);
            }
            toneWrapper.setdBwrappers(dBwrapperlist);
            tonelist.add(toneWrapper);
        }


        android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

        if (getDefaultLanguage().equalsIgnoreCase("")) {
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String countrycode = tm.getSimCountryIso();
            if (countrycode.equalsIgnoreCase("es")) {
                setDefaultLanguage(SPANISH);
            } else if (countrycode.equalsIgnoreCase("it")) {
                setDefaultLanguage(ITALIAN);
            } else {
                setDefaultLanguage(ENGLISH);
            }
        }
        if (getLanguage().equalsIgnoreCase(SPANISH)) {
            changeLocale(this, new Locale("es"));
        } else if (getLanguage().equalsIgnoreCase(ITALIAN)) {
            changeLocale(this, new Locale("it"));
        } else if (getLanguage().equalsIgnoreCase(ENGLISH)) {
            changeLocale(this, new Locale("en"));
        } else {
            if (getDefaultLanguage().equalsIgnoreCase(SPANISH)) {
                changeLocale(this, new Locale("es"));
            } else if (getDefaultLanguage().equalsIgnoreCase(ITALIAN)) {
                changeLocale(this, new Locale("it"));
            } else {
                changeLocale(this, new Locale("en"));
            }
        }

    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("DiallingCodes.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public boolean isApplicationBroughtToBackground() {
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(getPackageName())) {
                return true;
            }
        }
        return false;
    }

    public static void changeLocale(Context context, Locale locale) {
        Configuration conf = context.getResources().getConfiguration();
        conf.locale = locale;
        Locale.setDefault(locale);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            conf.setLayoutDirection(conf.locale);
        }
        context.getResources().updateConfiguration(conf, context.getResources().getDisplayMetrics());
    }

    // This is common method for Print Log Information message
    public static void PrintLogInfo(String activityname, String message) {

        Log.i(activityname, message);
    }

    public void setDeviceToken(String deviceToken) {
        editor = sharedPreferences.edit();
        editor.putString(DEVICETOKEN + "_" + Locale.getDefault().getLanguage(), deviceToken);
        editor.commit();
    }

    public String getDeviceToken() {

        if (Locale.getDefault().getLanguage().equalsIgnoreCase("es"))
            return sharedPreferences.getString(DEVICETOKEN + "_" + Locale.getDefault().getLanguage(), "123456");
        else {
            return sharedPreferences.getString(DEVICETOKEN + "_" + Locale.getDefault().getLanguage(), "");
        }
    }

    public void setDefaultLanguage(String language) {
        editor = sharedPreferences.edit();
        editor.putString(DEFAULTLANGUAGE, language);
        editor.commit();
    }

    public String getDefaultLanguage() {
        return sharedPreferences.getString(DEFAULTLANGUAGE, "");
    }

    public void setLanguage(String language) {
        editor = sharedPreferences.edit();
        editor.putString(LANGUAGE, language);
        editor.commit();
    }

    public String getLanguage() {
        return sharedPreferences.getString(LANGUAGE, ACCDEVICE);
    }

    public static Myapplication getInstance() {
        return ctx;
    }


    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (inputMethodManager.isAcceptingText()) {
                inputMethodManager.hideSoftInputFromWindow(activity
                        .getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void ShowMassage(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }

    public static void PrintLog(String log) {
        System.out.println(log);
    }

    public static void popErrorMsg(String titleMsg, String errorMsg,
                                   Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(AppName).setMessage(errorMsg)
                .setPositiveButton("OK", new OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();

        alert.show();

    }


    //
    public static void spinnerStart(Context context) {

        String pleaseWait = "Please wait...";
        dialog = ProgressDialog.show(context, "", pleaseWait, true);
    }


    public static void spinnerStop() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }


}
