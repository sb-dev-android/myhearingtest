package com.myhearing.app.Wrapper;

import java.util.ArrayList;

/**
 * Created by hitesh on 18/5/16.
 */
public class ToneWrapper {

    int toneFrequency;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    String mode = "";

    public ArrayList<DBwrapper> getdBwrappers() {
        return dBwrappers;
    }

    public void setdBwrappers(ArrayList<DBwrapper> dBwrappers) {
        this.dBwrappers = dBwrappers;
    }

    public int getToneFrequency() {
        return toneFrequency;
    }

    public void setToneFrequency(int toneFrequency) {
        this.toneFrequency = toneFrequency;
    }

    ArrayList<DBwrapper> dBwrappers;

}
