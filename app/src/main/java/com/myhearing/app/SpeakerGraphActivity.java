package com.myhearing.app;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.myhearing.app.CustomWidget.AppleBold;
import com.myhearing.app.CustomWidget.VerdanaRegular;
import com.myhearing.app.Fragment.Menufragment;
import com.navdrawer.SimpleSideDrawer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Hitesh Gehlot on 3/5/16.
 */
public class SpeakerGraphActivity extends BaseActivity {


    @Bind(R.id.tablelayout)
    TableLayout tablelayout;
    @Bind(R.id.containerlayout)
    RelativeLayout containerlayout;

    Context context;
    @Bind(R.id.row50_1)
    TextView row501;
    @Bind(R.id.row50_2)
    TextView row502;
    @Bind(R.id.row50_4)
    TextView row504;
    @Bind(R.id.row50_8)
    TextView row508;
    @Bind(R.id.row40_1)
    TextView row401;
    @Bind(R.id.row40_2)
    TextView row402;
    @Bind(R.id.row40_4)
    TextView row404;
    @Bind(R.id.row40_8)
    TextView row408;
    @Bind(R.id.row30_1)
    TextView row301;
    @Bind(R.id.row30_2)
    TextView row302;
    @Bind(R.id.row30_4)
    TextView row304;
    @Bind(R.id.row30_8)
    TextView row308;
    @Bind(R.id.row20_1)
    TextView row201;
    @Bind(R.id.row20_2)
    TextView row202;
    @Bind(R.id.row20_4)
    TextView row204;
    @Bind(R.id.row20_8)
    TextView row208;
    @Bind(R.id.row50)
    TableRow row50;
    @Bind(R.id.row40)
    TableRow row40;
    @Bind(R.id.row30)
    TableRow row30;
    @Bind(R.id.row20)
    TableRow row20;
    ImageView imageView;
    Drawable drawable;
    ArrayList<HashMap<String, Integer>> hashmaplist;
    @Bind(R.id.freq1)
    TextView freq1;
    @Bind(R.id.freq2)
    TextView freq2;
    @Bind(R.id.freq4)
    TextView freq4;
    @Bind(R.id.freq8)
    TextView freq8;
    @Bind(R.id.graphlayout)
    LinearLayout graphlayout;
    @Bind(R.id.result_text)
    VerdanaRegular resultText;
    @Bind(R.id.smiley_img)
    ImageView smileyImg;
    @Bind(R.id.btnshareresult)
    ImageView btnshareresult;
    Boolean result = true;
    @Bind(R.id.freq1R)
    TextView freq1R;
    @Bind(R.id.freq2R)
    TextView freq2R;
    @Bind(R.id.freq4R)
    TextView freq4R;
    @Bind(R.id.freq8R)
    TextView freq8R;
    @Bind(R.id.row50_1R)
    TextView row501R;
    @Bind(R.id.row50_2R)
    TextView row502R;
    @Bind(R.id.row50_4R)
    TextView row504R;
    @Bind(R.id.row50_8R)
    TextView row508R;
    @Bind(R.id.row50R)
    TableRow row50R;
    @Bind(R.id.row40_1R)
    TextView row401R;
    @Bind(R.id.row40_2R)
    TextView row402R;
    @Bind(R.id.row40_4R)
    TextView row404R;
    @Bind(R.id.row40_8R)
    TextView row408R;
    @Bind(R.id.row40R)
    TableRow row40R;
    @Bind(R.id.row30_1R)
    TextView row301R;
    @Bind(R.id.row30_2R)
    TextView row302R;
    @Bind(R.id.row30_4R)
    TextView row304R;
    @Bind(R.id.row30_8R)
    TextView row308R;
    @Bind(R.id.row30R)
    TableRow row30R;
    @Bind(R.id.row20_1R)
    TextView row201R;
    @Bind(R.id.row20_2R)
    TextView row202R;
    @Bind(R.id.row20_4R)
    TextView row204R;
    @Bind(R.id.row20_8R)
    TextView row208R;
    @Bind(R.id.row20R)
    TableRow row20R;
    @Bind(R.id.tablelayoutR)
    TableLayout tablelayoutR;
    @Bind(R.id.containerlayoutR)
    RelativeLayout containerlayoutR;


    public SimpleSideDrawer slide;
    @Bind(R.id.titletext)
    AppleBold titletext;

    @OnClick(R.id.img_menu)
    public void clickMenu() {
        slide.toggleLeftDrawer();

    }


    @OnClick(R.id.back_button)
    public void clickbackbutton() {
        finish();
    }

    @OnClick(R.id.home_button)
    public void clickhomebutton() {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void OnSuperSuccess(String response, String key) {

    }

    @Override
    public void OnSuperFailure(String errormsg) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speakergraph);
        ButterKnife.bind(this);
        context = this;
        slide = new SimpleSideDrawer(this);
        btnshareresult.setEnabled(false);
        btnshareresult.setAlpha(0.5f);
        titletext.setText(getResources().getString(R.string.results));
        slide.setLeftBehindContentView(R.layout.menu_layout);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.framelayout, new Menufragment());
        ft.commit();
        drawable = getResources().getDrawable(R.drawable.circlegreenshape);
        hashmaplist = (ArrayList<HashMap<String, Integer>>) getIntent().getSerializableExtra("hashMapslist");
        for (int i = 0; i < hashmaplist.size(); i++) {

            if (hashmaplist.get(i).containsKey("1000L")) {

                int db = hashmaplist.get(i).get("1000L");
                if (db == 50) {
                    result = false;
                } else if (db == 40) {
                    result = false;
                } else if (db == 30) {
                    result = false;
                } else if (db == 20) {
                }
                freq1.setText(db + "");
            } else if (hashmaplist.get(i).containsKey("2000L")) {

                int db = hashmaplist.get(i).get("2000L");
                if (db == 50) {
                    result = false;
                } else if (db == 40) {
                    result = false;
                } else if (db == 30) {
                    result = false;
                } else if (db == 20) {
                }
                freq2.setText(db + "");

            } else if (hashmaplist.get(i).containsKey("4000L")) {

                int db = hashmaplist.get(i).get("4000L");
                if (db == 50) {
                    result = false;

                } else if (db == 40) {
                    result = false;
                } else if (db == 30) {
                    result = false;

                } else if (db == 20) {
                }
                freq4.setText(db + "");

            } else if (hashmaplist.get(i).containsKey("8000L")) {

                int db = hashmaplist.get(i).get("8000L");
                if (db == 50) {
                    result = false;

                } else if (db == 40) {
                    result = false;

                } else if (db == 30) {
                    result = false;

                } else if (db == 20) {
                }
                freq8.setText(db + "");

            } else if (hashmaplist.get(i).containsKey("1000R")) {

                int db = hashmaplist.get(i).get("1000R");
                if (db == 50) {
                    result = false;
                } else if (db == 40) {
                    result = false;

                } else if (db == 30) {
                    result = false;

                } else if (db == 20) {
                }
                freq1R.setText(db + "");
            } else if (hashmaplist.get(i).containsKey("2000R")) {

                int db = hashmaplist.get(i).get("2000R");
                if (db == 50) {
                    result = false;

                } else if (db == 40) {
                    result = false;

                } else if (db == 30) {
                    result = false;

                } else if (db == 20) {
                }
                freq2R.setText(db + "");

            } else if (hashmaplist.get(i).containsKey("4000R")) {

                int db = hashmaplist.get(i).get("4000R");
                if (db == 50) {
                    result = false;
                } else if (db == 40) {
                    result = false;
                } else if (db == 30) {
                    result = false;
                } else if (db == 20) {
                }
                freq4R.setText(db + "");

            } else if (hashmaplist.get(i).containsKey("8000R")) {

                int db = hashmaplist.get(i).get("8000R");
                if (db == 50) {
                    result = false;

                } else if (db == 40) {
                    result = false;

                } else if (db == 30) {
                    result = false;

                } else if (db == 20) {
                }
                freq8R.setText(db + "");

            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i < hashmaplist.size(); i++) {

                    if (hashmaplist.get(i).containsKey("1000L")) {

                        int db = hashmaplist.get(i).get("1000L");
                        if (db == 50) {
                            draw50_1();
                            result = false;
                        } else if (db == 40) {
                            draw40_1();
                            result = false;

                        } else if (db == 30) {
                            draw30_1();
                            result = false;

                        } else if (db == 20) {
                            draw20_1();
                        }
                        freq1.setText(db + "");
                    } else if (hashmaplist.get(i).containsKey("2000L")) {

                        int db = hashmaplist.get(i).get("2000L");
                        if (db == 50) {
                            draw50_2();
                            result = false;
                        } else if (db == 40) {
                            draw40_2();
                            result = false;
                        } else if (db == 30) {
                            draw30_2();
                            result = false;
                        } else if (db == 20) {
                            draw20_2();
                        }
                        freq2.setText(db + "");

                    } else if (hashmaplist.get(i).containsKey("4000L")) {

                        int db = hashmaplist.get(i).get("4000L");
                        if (db == 50) {
                            draw50_4();
                            result = false;

                        } else if (db == 40) {
                            draw40_4();
                            result = false;

                        } else if (db == 30) {
                            draw30_4();
                            result = false;

                        } else if (db == 20) {
                            draw20_4();
                        }
                        freq4.setText(db + "");

                    } else if (hashmaplist.get(i).containsKey("8000L")) {

                        int db = hashmaplist.get(i).get("8000L");
                        if (db == 50) {
                            draw50_8();
                            result = false;

                        } else if (db == 40) {
                            draw40_8();
                            result = false;

                        } else if (db == 30) {
                            draw30_8();
                            result = false;

                        } else if (db == 20) {
                            draw20_8();
                        }
                        freq8.setText(db + "");

                    } else if (hashmaplist.get(i).containsKey("1000R")) {

                        int db = hashmaplist.get(i).get("1000R");
                        if (db == 50) {
                            draw50_1R();
                            result = false;
                        } else if (db == 40) {
                            draw40_1R();
                            result = false;

                        } else if (db == 30) {
                            draw30_1R();
                            result = false;

                        } else if (db == 20) {
                            draw20_1R();
                        }
                        freq1R.setText(db + "");
                    } else if (hashmaplist.get(i).containsKey("2000R")) {

                        int db = hashmaplist.get(i).get("2000R");
                        if (db == 50) {
                            draw50_2R();
                            result = false;

                        } else if (db == 40) {
                            draw40_2R();
                            result = false;

                        } else if (db == 30) {
                            draw30_2R();
                            result = false;

                        } else if (db == 20) {
                            draw20_2R();
                        }
                        freq2R.setText(db + "");

                    } else if (hashmaplist.get(i).containsKey("4000R")) {

                        int db = hashmaplist.get(i).get("4000R");
                        if (db == 50) {
                            draw50_4R();
                            result = false;
                        } else if (db == 40) {
                            draw40_4R();
                            result = false;
                        } else if (db == 30) {
                            draw30_4R();
                            result = false;
                        } else if (db == 20) {
                            draw20_4R();
                        }
                        freq4R.setText(db + "");

                    } else if (hashmaplist.get(i).containsKey("8000R")) {

                        int db = hashmaplist.get(i).get("8000R");
                        if (db == 50) {
                            draw50_8R();
                            result = false;

                        } else if (db == 40) {
                            draw40_8R();
                            result = false;

                        } else if (db == 30) {
                            draw30_8R();
                            result = false;

                        } else if (db == 20) {
                            draw20_8R();
                        }
                        freq8.setText(db + "");

                    }
                }


            }
        }, 1000);

        if (result) {
            resultText.setText(getString(R.string.This_test_shows_that_you_do_not));
//            resultText.setTextColor(getResources().getColor(R.color.green_text));
            smileyImg.setImageResource(R.mipmap.img_happy);
        } else {
//            resultText.setTextColor(getResources().getColor(R.color.red_text));
            resultText.setText(getString(R.string.This_test_shows_that_you_need));
            smileyImg.setImageResource(R.mipmap.img_sad);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkwritePermission();
                btnshareresult.setEnabled(true);
                btnshareresult.setAlpha(1.0f);
            }
        }, 3000);

    }

    private final int REQUEST_CODE_GALLERY = 0x1;

    private void checkwritePermission() {

        Log.d("mTest Permission Check", "Checking for Gallery permission");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(this, "Permission is needed for proper working of this app", Toast.LENGTH_SHORT).show();
            } else {
                ActivityCompat.requestPermissions(
                        this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_CODE_GALLERY);
            }
        } else {
            getscreenshot();


        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE_GALLERY) {

            getscreenshot();


        }
    }

    private void getscreenshot() {
        Bitmap icon = screenShot(graphlayout);
//                imageview.setImageBitmap(screenShot(graphlayout));
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        screenshot = new File(Environment.getExternalStorageDirectory()
                + File.separator + System.currentTimeMillis() + "_screenshot.jpg");
        try {
            screenshot.createNewFile();
            FileOutputStream fo = new FileOutputStream(screenshot);
            fo.write(bytes.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    File screenshot;

    public Bitmap screenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }


    @OnClick(R.id.btnshareresult)
    public void clickShare() {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        share.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text));
        share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_subject));
        share.putExtra(Intent.EXTRA_STREAM,
                Uri.parse("file:///" + screenshot.getPath()));
        startActivity(Intent.createChooser(share, "Share Result"));
    }


    private void draw20_8() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlegreenshape);
        imageView.setX(tablelayout.getX() + row208.getX() + row208.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row20.getY() + row208.getY() + row208.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayout.addView(imageView);
    }

    private void draw20_8R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlegreenshape);
        imageView.setX(tablelayout.getX() + row208R.getX() + row208R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row20R.getY() + row208R.getY() + row208R.getHeight() - drawable.getMinimumHeight() / 2);
        containerlayoutR.addView(imageView);
    }

    private void draw20_4() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlegreenshape);
        imageView.setX(tablelayout.getX() + row204.getX() + row204.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row20.getY() + row204.getY() + row204.getHeight() - drawable.getMinimumHeight() / 2);
        containerlayout.addView(imageView);
    }

    private void draw20_4R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlegreenshape);
        imageView.setX(tablelayout.getX() + row204R.getX() + row204R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row20R.getY() + row204R.getY() + row204R.getHeight() - drawable.getMinimumHeight() / 2);
        containerlayoutR.addView(imageView);
    }

    private void draw20_2() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlegreenshape);
        imageView.setX(tablelayout.getX() + row202.getX() + row202.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row20.getY() + row202.getY() + row202.getHeight() - drawable.getMinimumHeight() / 2);
        containerlayout.addView(imageView);
    }

    private void draw20_2R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlegreenshape);
        imageView.setX(tablelayout.getX() + row202R.getX() + row202R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row20R.getY() + row202R.getY() + row202R.getHeight() - drawable.getMinimumHeight() / 2);
        containerlayoutR.addView(imageView);
    }

    private void draw20_1() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlegreenshape);
        imageView.setX(tablelayout.getX() + row201.getX() + row201.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row20.getY() + row201.getY() + row201.getHeight() - drawable.getMinimumHeight() / 2);
        containerlayout.addView(imageView);
    }

    private void draw20_1R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlegreenshape);
        imageView.setX(tablelayout.getX() + row201R.getX() + row201R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row20R.getY() + row201R.getY() + row201R.getHeight() - drawable.getMinimumHeight() / 2);
        containerlayoutR.addView(imageView);
    }

    private void draw30_8() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlepinkshape);
        imageView.setX(tablelayout.getX() + row308.getX() + row308.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row30.getY() + row308.getY() + row308.getHeight() - drawable.getMinimumHeight() / 2);
        containerlayout.addView(imageView);
    }

    private void draw30_8R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlepinkshape);
        imageView.setX(tablelayout.getX() + row308R.getX() + row308R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row30R.getY() + row308R.getY() + row308R.getHeight() - drawable.getMinimumHeight() / 2);
        containerlayoutR.addView(imageView);
    }

    private void draw30_4() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlepinkshape);
        imageView.setX(tablelayout.getX() + row304.getX() + row304.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row30.getY() + row304.getY() + row304.getHeight() - drawable.getMinimumHeight() / 2);
        containerlayout.addView(imageView);
    }

    private void draw30_4R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlepinkshape);
        imageView.setX(tablelayout.getX() + row304R.getX() + row304R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row30R.getY() + row304R.getY() + row304R.getHeight() - drawable.getMinimumHeight() / 2);
        containerlayoutR.addView(imageView);
    }

    private void draw30_2() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlepinkshape);
        imageView.setX(tablelayout.getX() + row302.getX() + row302.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row30.getY() + row302.getY() + row302.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayout.addView(imageView);

    }

    private void draw30_2R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlepinkshape);
        imageView.setX(tablelayout.getX() + row302R.getX() + row302R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row30R.getY() + row302R.getY() + row302R.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayoutR.addView(imageView);

    }

    private void draw30_1() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlepinkshape);
        imageView.setX(tablelayout.getX() + row301.getX() + row301.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row30.getY() + row301.getY() + row301.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayout.addView(imageView);

    }

    private void draw30_1R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circlepinkshape);
        imageView.setX(tablelayout.getX() + row301R.getX() + row301R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row30R.getY() + row301R.getY() + row301R.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayoutR.addView(imageView);

    }

    private void draw40_8() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleorangeshape);
        imageView.setX(tablelayout.getX() + row408.getX() + row408.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row40.getY() + row408.getY() + row408.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayout.addView(imageView);

    }

    private void draw40_8R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleorangeshape);
        imageView.setX(tablelayout.getX() + row408R.getX() + row408R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row40R.getY() + row408R.getY() + row408R.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayoutR.addView(imageView);

    }

    private void draw40_4() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleorangeshape);
        imageView.setX(tablelayout.getX() + row404.getX() + row404.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row40.getY() + row404.getY() + row404.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayout.addView(imageView);

    }

    private void draw40_4R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleorangeshape);
        imageView.setX(tablelayout.getX() + row404R.getX() + row404R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row40R.getY() + row404R.getY() + row404R.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayoutR.addView(imageView);

    }

    private void draw40_2() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleorangeshape);
        imageView.setX(tablelayout.getX() + row402.getX() + row402.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row40.getY() + row402.getY() + row402.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayout.addView(imageView);

    }

    private void draw40_2R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleorangeshape);
        imageView.setX(tablelayout.getX() + row402R.getX() + row402R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row40R.getY() + row402R.getY() + row402R.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayoutR.addView(imageView);

    }

    private void draw40_1() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleorangeshape);
        imageView.setX(tablelayout.getX() + row401.getX() + row401.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row40.getY() + row401.getY() + row401.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayout.addView(imageView);

    }

    private void draw40_1R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleorangeshape);
        imageView.setX(tablelayout.getX() + row401R.getX() + row401R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row40R.getY() + row401R.getY() + row401R.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayoutR.addView(imageView);

    }

    private void draw50_8() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleredshape);
        imageView.setX(tablelayout.getX() + row508.getX() + row508.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row50.getY() + row508.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayout.addView(imageView);

    }

    private void draw50_8R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleredshape);
        imageView.setX(tablelayout.getX() + row508R.getX() + row508R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row50R.getY() + row508R.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayoutR.addView(imageView);

    }

    private void draw50_4() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleredshape);
        imageView.setX(tablelayout.getX() + row504.getX() + row504.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row50.getY() + row504.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayout.addView(imageView);

    }

    private void draw50_4R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleredshape);
        imageView.setX(tablelayout.getX() + row504R.getX() + row504R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row50R.getY() + row504R.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayoutR.addView(imageView);

    }

    private void draw50_2() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleredshape);
        imageView.setX(tablelayout.getX() + row502.getX() + row502.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row502.getY() + row502.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayout.addView(imageView);

    }

    private void draw50_2R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleredshape);
        imageView.setX(tablelayout.getX() + row502R.getX() + row502R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row502R.getY() + row502R.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayoutR.addView(imageView);

    }

    private void draw50_1() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleredshape);
        imageView.setX(tablelayout.getX() + row501.getX() + row501.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row501.getY() + row501.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayout.addView(imageView);
    }

    private void draw50_1R() {
        imageView = new ImageView(context);
        imageView.setImageResource(R.drawable.circleredshape);
        imageView.setX(tablelayout.getX() + row501R.getX() + row501R.getWidth() - drawable.getMinimumWidth() / 2);
        imageView.setY(tablelayout.getY() + row501R.getY() + row501R.getHeight() - drawable.getMinimumHeight() / 2);

        containerlayoutR.addView(imageView);
    }

}
