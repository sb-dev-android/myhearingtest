package com.myhearing.app;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.myhearing.app.CustomWidget.AppleBold;
import com.myhearing.app.Fragment.Menufragment;
import com.navdrawer.SimpleSideDrawer;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Hitesh Gehlot on 4/5/16.
 */
public class DeviceSpeakerTutorialActivity extends BaseActivity {


    public SimpleSideDrawer slide;
    @Bind(R.id.videoview)
    VideoView videoview;
    @Bind(R.id.videolayout)
    RelativeLayout videolayout;
    @Bind(R.id.titletext)
    AppleBold titletext;

    @OnClick(R.id.btncontinue)
    public void clickButtonContinue() {
        Intent intent = new Intent(DeviceSpeakerTutorialActivity.this, SpeakerTestActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.img_menu)
    public void clickMenu() {
        slide.toggleLeftDrawer();
    }

    @OnClick(R.id.back_button)
    public void clickbackbutton() {
        finish();
    }

    @OnClick(R.id.home_button)
    public void clickhomebutton() {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }


    @Override
    public void OnSuperSuccess(String response, String key) {

    }

    @Override
    public void OnSuperFailure(String errormsg) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devicespeakertutorial);
        ButterKnife.bind(this);
        slide = new SimpleSideDrawer(this);
        titletext.setText(getResources().getString(R.string.test));
        slide.setLeftBehindContentView(R.layout.menu_layout);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.framelayout, new Menufragment());
        ft.commit();
        String uriPath = "android.resource://" + getPackageName() + "/raw/my_hearing_man2";
        Uri UrlPath = Uri.parse(uriPath);
        videoview.setVideoURI(UrlPath);
        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                videolayout.setVisibility(View.VISIBLE);
                videoview.start();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        videoview.pause();
    }


}
