package com.myhearing.app.WebTasks;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.myhearing.app.Application.Myapplication;
import com.myhearing.app.R;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by hitesh on 2/7/16.
 */
public class WebServiceApi {

    public static final String BASE_URL = "http://mdataapps.com/my_hearing/index.php/api/";
    //key for call apis
    public static final String KEY_USER_TOKEN = "user_token";
    public static final String KEY_APP_ANALYSIS = "app_analysis";
    //define api suburl
    public static final String API_USER_TOKEN = "myhearing_v2/use_token";
    public static final String API_APP_ANALYSIS = "myhearing_v2/app_analysis";

    AsyncHttpClient asyncHttpClient;
    WebServiceCallBack webServiceCallBack;
    Context context;

    public WebServiceApi(WebServiceCallBack webServiceCallBack, Context context) {
        asyncHttpClient = new AsyncHttpClient();
        this.context = context;
        this.webServiceCallBack = webServiceCallBack;
        asyncHttpClient.setTimeout(30 * 1000);
        asyncHttpClient.setResponseTimeout(30 * 1000);
        asyncHttpClient.setConnectTimeout(30 * 1000);
    }


    public void PostMethod(final String KeyValue, String subApi, RequestParams requestParams, Boolean IsDialogShow) {
        if (!Myapplication.cd.isConnectingToInternet()) {
            webServiceCallBack.OnFailure(context.getResources().getString(R.string.error_internet));
            return;
        }
        if (IsDialogShow)
            Myapplication.spinnerStart(context);

        asyncHttpClient.post(context, BASE_URL + subApi, requestParams,
                new TextHttpResponseHandler() {

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Myapplication.spinnerStop();
                        webServiceCallBack.OnFailure(context.getResources().getString(R.string.something_wrong));
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        Myapplication.spinnerStop();
                        Myapplication.PrintLogInfo("Api", "call api response:" + responseString);
                        webServiceCallBack.OnSuccess(responseString, KeyValue);
                    }
                }
        );

    }


}
