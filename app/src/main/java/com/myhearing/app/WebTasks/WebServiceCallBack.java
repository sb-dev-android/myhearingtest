package com.myhearing.app.WebTasks;

/**
 * Created by hitesh on 2/7/16.
 */
public interface WebServiceCallBack {

    public void OnSuccess(String response, String key);

    public void OnFailure(String errormsg);
}
